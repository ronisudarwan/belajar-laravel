@extends('layout.master')

@section('judul')
	Halaman Form Pendaftaran
@endsection
@section('content')
	<h2>Buat Account Baru</h2>
	<h3>Sign Up Form</h3>
	
	<form action="/welcome" method="post">
        @csrf
        <label>First Name</label> <br>
		<input type="text" name="fname">
		<br><br>
		<label>Last Name</label><br>
		<input type="text" name="lname">
		<br><br>
		<label>Gender</label><br>
		<input type="radio"> Male <br>
		<input type="radio"> Female <br>
		<br>
		<label>Nationality</label> <br>
		<select name="Nationality">
			<option value="Indonesia">Indonesia</option>
			<option value="Amerika">Amerika</option>
			<option value="Inggris">Inggris</option>
		</select>
		<br><br>
		<label>Language Spoken</label> <br>
		<input type="checkbox" name="Indonesia"> Bahasa Indonesia <br>
		<input type="checkbox" name="English"> English <br>
		<input type="checkbox" name="Other"> Other <br>
		<br>
		<label>Bio</label> <br>
		<textarea name="message" rows="10" cols="30"></textarea>
		<br>
		<input type="submit" value="Sign Up">
	</form>	
@endsection