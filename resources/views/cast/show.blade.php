@extends('layout.master')

@section('judul')
	Detail Pemain
@endsection

@section('content')
    <h1>{{$cast->nama}}</h1>
    <p>{{$cast->umur}} Tahun</p>
    <p>{{$cast->biodata}}</p>
    <a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>
@endsection