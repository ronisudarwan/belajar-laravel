@extends('layout.master')

@section('judul')
	List Pemain Cast
@endsection

@section('content')
    
    <a href="/cast/create" class="btn btn-primary my-3">Tambah Pemain</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key=>$item)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$item->nama}}</td>
                    <td>
                        <form action="/cast/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td> Data Tidak Ada </td>
                </tr>    
            @endforelse
            
        </tbody>
      </table>
@endsection