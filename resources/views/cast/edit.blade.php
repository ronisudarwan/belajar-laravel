@extends('layout.master')

@section('judul')
	Edit Cast
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put');
    <div class="form-group">
      <label >Nama Pemain</label>
      <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Umur Pemain</label>
      <input type="number" name="umur" class="form-control" value="{{$cast->umur}}" >
    </div>
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Biodata pemain</label>
        <textarea name="biodata" class="form-control" cols="30" rows="10">{{$cast->biodata}}</textarea>
    </div>
    @error('biodata')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection