<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/account','AuthController@biodata');
Route::post('/welcome','AuthController@kirimdata');

Route::get('/data', function(){
    return view('halaman.data');
});

Route::get('/data-table', function(){
    return view('halaman.datatable');
});

//CRUD Route

//Create Data
//menampilkan form untuk membuat data pemain film baru 
Route::get('/cast/create', 'CastController@create');

//menyimpan data baru ke database
Route::post('/cast', 'CastController@store');

//read data
//route untuk nampilkan semua data ke database
Route::get('/cast', 'CastController@index');

//route detail cast pemain
Route::get('/cast/{cast_id}', 'CastController@show');


//Updated data 
//mengarah ke kolom edit cast yang membawa parameter
Route::get('/cast/{cast_id}/edit','CastController@edit');

//update data di table kategori berdasarkan id
Route::put('/cast/{cast_id}','CastController@update');

//Delete Data
//menghapus data cast berdasarkan id
Route::delete('/cast/{cast_id}','CastController@destroy');