<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function biodata()
    {
        return view('halaman.account');
    }

    public function kirimdata(Request $request)
    {
        // dd($request->all()); //untuk ngetes
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];

        return view('halaman.welcome', compact('namaDepan','namaBelakang'));
    }
}
